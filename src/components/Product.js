// modules
import React, { Component } from 'react';
// import { Badge } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faQuestion, faPaintBrush, faCode, faBezierCurve, faCamera, faVideo, faCube, faMusic, faGraduationCap } from '@fortawesome/free-solid-svg-icons';
// elements
import Player from './elements/Player';

class Product extends Component {

    state = {
        icon: faQuestion
    }

    getIcon() {

        switch (this.props.product.type) {
            case "themes & templates":
                if (this.props.product.classification.indexOf("courses") > -1) {
                    this.setState({ icon: faGraduationCap })
                } else {
                    this.setState({ icon: faPaintBrush })
                }
                break;
            case "code":
                this.setState({ icon: faCode })
                break;
            case "graphics":
                this.setState({ icon: faBezierCurve })
                break;
            case "photos":
                this.setState({ icon: faCamera })
                break;
            case "videos":
                this.setState({ icon: faVideo })
                break;
            case "3d files":
                this.setState({ icon: faCube })
                break;
            case "audio":
                this.setState({ icon: faMusic })
                break;
            case "courses":
                this.setState({ icon: faGraduationCap })
                break;
            default: 
                this.setState({ icon: faQuestion })
        }

    }

    componentDidMount() {
        this.getIcon();
    }

    render() {

        const {
            id,
            title,
            thumb,
            author,
            author_image,
            // url,
            price,
            // site,
            type,
            classification,
            // tags
        } = this.props.product;

        let cleanType = type.replace(/&/g, '').replace(/templates/g, '').replace(/files/g, '');
        if (classification.indexOf("courses") > -1) {
            cleanType = 'courses';
        }

        return (
            <React.Fragment>
                <div className={"loadr-cover loadr-cover__" + cleanType}>
                    <h1>{id}</h1>
                    <div className="loadr-cover--image">
                        {(() => {
                            if (cleanType === 'audio')
                                return (
                                    <React.Fragment>
                                        <Player audio={thumb} />
                                        { author_image ? (
                                            <div className="loadr-cover--image--author">
                                                <img src={author_image} alt={title} />
                                            </div>
                                        ) : null }
                                    </React.Fragment>
                                )
                            else
                                return <img src={thumb} alt={title} />
                        })()}
                        <div className="loadr-cover--image--icon">
                            <FontAwesomeIcon icon={this.state.icon} />
                        </div>
                    </div>
                    <div className="loadr-cover--info">
                        <h6>{title}</h6>
                        <span className="byline">by {author}</span>
                        <span className="price">{'$' + price}</span>
                        {/* <sub>{ url }</sub><br/>
                        <Badge pill className={"badge-site badge-" + site}>{site}</Badge>
                        <Badge pill variant="dark" className="badge-type">{type}</Badge><br/>
                        {
                            classification.map((classif, index) => {
                                return (
                                    <Badge key={index} pill variant="secondary" className="badge-classification">{classif}</Badge>
                                )
                            })
                        }
                        <br/>
                        <div className="tag-cloud">
                            {
                                tags.map((tag, index) => {
                                    return (
                                        <sub key={index}>{tag}</sub>
                                    )
                                })
                            }
                        </div> */}
                    </div>
                </div>
            </React.Fragment>
        )

    }

}

export default Product;
