// modules
import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlay, faPause } from '@fortawesome/free-solid-svg-icons';

class Player extends Component {

    state = {
        playing: false
    }

    handlePlay = () => {
        this.player.play();
        this.setState({ playing: true });
    }

    handlePause = () => {
        this.player.pause();
        this.setState({ playing: false });
    }

    render() {

        const {
            audio
        } = this.props;

        return (
            <div className={'player playing__' + this.state.playing}>
                <button className="play" onClick={this.handlePlay}>
                    <FontAwesomeIcon icon={faPlay} />
                </button>
                <button className="pause" onClick={this.handlePause}>
                    <FontAwesomeIcon icon={faPause} />
                </button>
                <audio ref={(input) => {this.player = input}} src={audio} onEnded={this.handlePause} />
            </div>
        )
    }

}

export default Player;
