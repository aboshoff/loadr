// modules
import React, { Component } from 'react';

class Loader extends Component {

    state = {
        count: 8
    }

    render() {
        return (
            <div className="loadr-loader">
                {
                    Array.from(Array(this.state.count)).map((x, index) =>
                        <div key={index} className="loadr-loader--cube"></div>
                    )
                }
            </div>
        )
    }
}

export default Loader;
