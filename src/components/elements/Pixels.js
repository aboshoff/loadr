// modules
import React, { Component } from 'react';

class Pixels extends Component {

    state = {
        liCount: 20,
        pixelCount: 10
    }

    render() {

        const {
            color
        } = this.props;

        return (
            <ul className={"tiles tiles--" + color}>
                {
                    Array.from(Array(this.state.liCount)).map((x, index) =>
                        <li key={index}>
                            {
                                Array.from(Array(this.state.pixelCount)).map((x, index) =>
                                    <div key={index} className="pixel"></div>
                                )
                            }
                        </li>
                    )
                }
            </ul>
        )

    }

}
export default Pixels;
