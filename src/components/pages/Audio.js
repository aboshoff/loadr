// modules
import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
// components
import Pixels from './../elements/Pixels';

const Audio = () => {
    return (
        <React.Fragment>
            <div className="loadr-banner">
                <Pixels color="audio" />
                <Container fluid={true} className="loadr-banner--text">
                    <Row>
                        <Col>
                            <h1>Audio</h1>
                        </Col>
                    </Row>
                </Container>
            </div>
        </React.Fragment>
    )
}

export default Audio;