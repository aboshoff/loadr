// modules
import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
// components
import Pixels from './../elements/Pixels';

const Graphics = () => {
    return (
        <React.Fragment>
            <div className="loadr-banner">
                <Pixels color="graphics" />
                <Container fluid={true} className="loadr-banner--text">
                    <Row>
                        <Col>
                            <h1>Graphics</h1>
                        </Col>
                    </Row>
                </Container>
            </div>
        </React.Fragment>
    )
}

export default Graphics;