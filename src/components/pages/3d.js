// modules
import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
// components
import Pixels from './../elements/Pixels';

const ThreeD = () => {
    return (
        <React.Fragment>
            <div className="loadr-banner">
                <Pixels color="3d" />
                <Container fluid={true} className="loadr-banner--text">
                    <Row>
                        <Col>
                            <h1>3D Files</h1>
                        </Col>
                    </Row>
                </Container>
            </div>
        </React.Fragment>
    )
}

export default ThreeD;