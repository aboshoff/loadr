// modules
import React, { Component } from 'react';
import { withRouter, NavLink } from 'react-router-dom';
import { Row, Col, ButtonGroup, Button } from 'react-bootstrap';
import imagesLoaded from 'imagesloaded';
import { ScrollTo } from 'scroll-to-position';
import Bricks from 'bricks.js';
// temporary list
// import { productsEnvato } from './../temp/TempEnvatoList';
// components
import Product from './Product';
// elements
import Loader from './elements/Loader';

class ProductList extends Component {

	state = {
		error: null,
		isLoading: true,
		products: [],
		brickSize: 'large',
		productCountSmall: 70,
		productCountLarge: 35,
		pageProductLength: null
		// products: productsEnvato,
	}

	// function:: initiate bricks
	initBricks = () => {

		if (this.props.location.pathname === '/audio') {

			this.setState({ brickSize: 'small' })
			
			const smallBricksInstance = Bricks({
						container: '.bricks-grid',
						packed: 'data-packed',
						sizes: [
							{ columns: 1, gutter: 10 },
							{ mq: '320px', columns: 2, gutter: 10 },
							{ mq: '480px', columns: 3, gutter: 20 },
							{ mq: '640px', columns: 4, gutter: 20 },
							{ mq: '800px', columns: 5, gutter: 20 },
							{ mq: '960px', columns: 6, gutter: 20 },
							{ mq: '1120px', columns: 7, gutter: 20 },
							{ mq: '1280px', columns: 8, gutter: 20 },
							{ mq: '1440px', columns: 9, gutter: 20 },
							{ mq: '1600px', columns: 10, gutter: 20 },
							{ mq: '1760px', columns: 11, gutter: 20 },
							{ mq: '1920px', columns: 12, gutter: 20 },
							{ mq: '2080px', columns: 13, gutter: 20 },
							{ mq: '2240px', columns: 14, gutter: 20 }
						],
						position: false
			});

			if (document.readyState === "complete") {
				smallBricksInstance.pack().update().resize(true);

				this.setState({ isLoading: false });
			}

		} else {

			this.setState({ brickSize: 'large' })

			const largeBricksInstance = Bricks({
						container: '.bricks-grid',
						packed: 'data-packed',
						sizes: [
							{ columns: 1, gutter: 10 },
							{ mq: '320px', columns: 1, gutter: 10 },
							{ mq: '480px', columns: 1, gutter: 20 },
							{ mq: '640px', columns: 2, gutter: 20 },
							{ mq: '800px', columns: 2, gutter: 20 },
							{ mq: '960px', columns: 3, gutter: 20 },
							{ mq: '1120px', columns: 3, gutter: 20 },
							{ mq: '1280px', columns: 3, gutter: 20 },
							{ mq: '1440px', columns: 4, gutter: 20 },
							{ mq: '1600px', columns: 4, gutter: 20 },
							{ mq: '1760px', columns: 5, gutter: 20 },
							{ mq: '1920px', columns: 5, gutter: 20 },
							{ mq: '2080px', columns: 5, gutter: 20 },
							{ mq: '2240px', columns: 5, gutter: 20 }
						],
						position: false
			});
			
			if (document.readyState === "complete") {
				largeBricksInstance.pack().update().resize(true);

				this.setState({ isLoading: false });
			}

		}
			
	}

	// function:: check if all images have loaded
	checkImages = () => {

		imagesLoaded(document.querySelector('.bricks-grid'), function( instance ) {

			this.initBricks();

		}.bind(this));

	}

	// function:: updated products and sort
	updateProducts(prods) {

		function compare(a, b) {
			if ( a.published < b.published ) {
				return -1;
			}
			if ( a.published > b.published ) {
				return 1;
			}
			return 0;
		}

		// new array for altered products
		let productsArray = [];

		// product class
		class Product {
			constructor(id, title, published, url, price, classification, tags, thumb, author, author_image) {
				this.id = id;
				this.title = title;
				this.published = published;
				this.url = url;
				this.price = price / 100;
				this.site = this.url.includes('www') ? this.url.split('//')[1].split('/')[0].split('www.')[1].split('.')[0] : this.url.split('//')[1].split('/')[0].split('.')[0];
				switch (this.site) {
					// envato
					case "3docean":
						this.type = '3d files';
						this.thumb = thumb.icon_with_square_preview.image_urls[0].url;
					break;
					case "audiojungle":
						this.type = 'audio';
						// this.thumb = thumb.icon_with_audio_preview.icon_url;
						this.thumb = thumb.icon_with_audio_preview.mp3_preview_download_url;
					break;
					case "codecanyon":
						this.type = 'code';
						this.thumb = thumb.landscape_preview.landscape_url;
					break;
					case "graphicriver":
						this.type = 'graphics';
						this.thumb = thumb.icon_with_square_preview.image_urls[0].url;
					break;
					case "photodune":
						this.type = 'photos';
						this.thumb = thumb.icon_with_thumbnail_preview.thumbnail_url;
					break;
					case "themeforest":
						this.type = classification === 'courses/web-design' ? 'courses' : 'themes & templates';
						this.thumb = (classification === 'courses/web-design' || classification === 'courses/code') ? thumb.icon_with_video_preview.landscape_url : thumb.icon_with_landscape_preview.landscape_url;
					break;
					case "videohive":
						this.type = 'videos';
						this.thumb = thumb.icon_with_video_preview.landscape_url;
					break;
					// default
					default:
						this.type = 'still to do';
						this.thumb = 'https://via.placeholder.com/300x300.png?text=This+product+does+not+have+an+image.';
					}
				this.classification = classification.split('/');
				this.tags = tags;
				this.author = author;
				this.author_image = author_image;
			}
		}

		// push redefined products to new array
		const productsPromise = prods.map(product => {
			return productsArray.push(
				new Product (
					product.id,
					product.name,
					product.published_at,
					product.url,
					product.price_cents,
					product.classification,
					product.tags,
					product.previews,
					product.author_username,
					product.author_image,
				)
			)
		});

		// set the new states
		Promise.all(productsPromise)
			.then(
				this.setState({
					products: productsArray.sort(compare)
				})
			)
			.then(
				this.setState({
					pageProductLength: this.state.products.length
				}),
				this.checkImages()
			)

	}

	// function:: merge products
  mergeProducts(arr1, arr2, arr3, arr4, arr5, arr6, arr7) {

    const combined = [...arr1, ...arr2, ...arr3, ...arr4, ...arr5, ...arr6, ...arr7];

    this.updateProducts(combined);

  }

	// function:: search Envato
  searchEnvato(site, category, sort) {

		const ENVATO_BASE_URL = 'https://api.envato.com/v1/',
					ENVATO_SEARCH = 'discovery/search/search/item?',
					ENVATO_KEY = 'iCtDYBO82MFMr4wOGPoNLpBXJ3mjhCB2';

		let sortBy = sort ? sort : 'date',
				sortDirection = 'desc',
				domain = `${site}.net&`,
				pageSize = site === 'all' ? 5 : (site === 'audiojungle') ? this.state.productCountSmall : this.state.productCountLarge,
				pageNumber = this.props.pageNum ? this.props.pageNum : 1,
				ofCategory = category === 'none' ? '' : `&category=${category}&`;

		if (site === 'all') {

			Promise.all([
				fetch(`${ENVATO_BASE_URL}${ENVATO_SEARCH}site=codecanyon.net${ofCategory}&sort_by=${sortBy}&sort_direction=${sortDirection}&page_size=${pageSize}&page=${pageNumber}`, {
					headers: { 'Authorization': 'Bearer ' + ENVATO_KEY }
				}),
				fetch(`${ENVATO_BASE_URL}${ENVATO_SEARCH}site=themeforest.net${ofCategory}&sort_by=${sortBy}&sort_direction=${sortDirection}&page_size=${pageSize}&page=${pageNumber}`, {
					headers: { 'Authorization': 'Bearer ' + ENVATO_KEY }
				}),
				fetch(`${ENVATO_BASE_URL}${ENVATO_SEARCH}site=videohive.net${ofCategory}&sort_by=${sortBy}&sort_direction=${sortDirection}&page_size=${pageSize}&page=${pageNumber}`, {
					headers: { 'Authorization': 'Bearer ' + ENVATO_KEY }
				}),
				fetch(`${ENVATO_BASE_URL}${ENVATO_SEARCH}site=audiojungle.net${ofCategory}&sort_by=${sortBy}&sort_direction=${sortDirection}&page_size=${pageSize}&page=${pageNumber}`, {
					headers: { 'Authorization': 'Bearer ' + ENVATO_KEY }
				}),
				fetch(`${ENVATO_BASE_URL}${ENVATO_SEARCH}site=graphicriver.net${ofCategory}&sort_by=${sortBy}&sort_direction=${sortDirection}&page_size=${pageSize}&page=${pageNumber}`, {
					headers: { 'Authorization': 'Bearer ' + ENVATO_KEY }
				}),
				fetch(`${ENVATO_BASE_URL}${ENVATO_SEARCH}site=photodune.net${ofCategory}&sort_by=${sortBy}&sort_direction=${sortDirection}&page_size=${pageSize}&page=${pageNumber}`, {
					headers: { 'Authorization': 'Bearer ' + ENVATO_KEY }
				}),
				fetch(`${ENVATO_BASE_URL}${ENVATO_SEARCH}site=3docean.net${ofCategory}&sort_by=${sortBy}&sort_direction=${sortDirection}&page_size=${pageSize}&page=${pageNumber}`, {
					headers: { 'Authorization': 'Bearer ' + ENVATO_KEY }
				})
			])
			.then(([res1, res2, res3, res4, res5, res6, res7]) => Promise.all([
				res1.json(),
				res2.json(),
				res3.json(),
				res4.json(),
				res5.json(),
				res6.json(),
				res7.json()
			]))
			.then(([data1, data2, data3, data4, data5, data6, data7]) =>
				this.mergeProducts(data1.matches, data2.matches, data3.matches, data4.matches, data5.matches, data6.matches, data7.matches)
			)
			.catch(error => this.setState({ error }));
			
		} else {

			fetch(`${ENVATO_BASE_URL}${ENVATO_SEARCH}site=${domain}${ofCategory}&sort_by=${sortBy}&sort_direction=${sortDirection}&page_size=${pageSize}&page=${pageNumber}`, {
				headers: {
					'Authorization': 'Bearer ' + ENVATO_KEY
				}
			})
			.then(response => response.json())
			.then(data =>
				this.updateProducts(data.matches),
			)
			.catch(error => this.setState({ error }));

		}

	}

	// function:: search trigger
  searchTrigger(path) {

    switch (path) {
      // all
      case "/":
        this.searchEnvato('all', 'none', 'date');
        break;
      // themes & templates
      case "/themes-and-templates":
        this.searchEnvato('themeforest', 'none', 'date');
        break;
      // code
      case "/code":
        this.searchEnvato('codecanyon', 'none', 'date');
        break;
      // graphics
      case "/graphics":
        this.searchEnvato('graphicriver', 'none', 'date');
        break;
      // photos
      case "/photos":
        this.searchEnvato('photodune', 'none', 'date');
        break;
      // videos
      case "/videos":
        this.searchEnvato('videohive', 'none', 'date');
        break;
      // graphics
      case "/3d-files":
        this.searchEnvato('3docean', 'none', 'date');
        break;
      // audio
      case "/audio":
        this.searchEnvato('audiojungle', 'none', 'date');
        break;
      // courses
      case "/courses":
        this.searchEnvato('themeforest', 'courses', 'date');
        break;
      // default
      default:
        this.searchEnvato('all', 'none', 'date');
    }

	}

	// component did mount
	componentDidMount() {

		this.searchTrigger(this.props.location.pathname);
			
	}
	
	// component did update
	componentDidUpdate(prevProps) {

		// if the path has updated
		if (this.props.location !== prevProps.location) {

			console.log('ProductList.js - component did update and location changed');
			this.setState({ isLoading: true });
			this.searchTrigger(this.props.location.pathname);
			ScrollTo(document.querySelector('.loadr-products-canvas'), {
				cancelOnUserScroll: false,
				offset: [0, -134],
				duration: [100, 2000]
			});

		} else {
				console.log('ProductList.js - component did update but with no changes');
		}

	}

	render() {

		const {
			error,
			isLoading,
			products,
			brickSize,
			productCountSmall,
			productCountLarge,
			pageProductLength
		} = this.state;

		const {
			pageNum,
			location
		} = this.props;

		console.log('is loading:', isLoading);

		return (
			<React.Fragment>
				<Row>
					<Col>
						{/* display error message if encountered */}
        		{ error ? <p>{error.message}</p>: null }
						{/* show loader when loading */}
        		{ isLoading ? ( <Loader /> ) : null }
						<div className="bricks-grid" style={!isLoading ? ({opacity: 1}) : ({opacity: 0})}>
							{
								products.map(product => {
									if (!product.thumb.startsWith('/'))
										return (
											<div key={product.id} className={"bricks-grid--item bricks-grid--item__" + brickSize}>
												<Product product={product} />
											</div>
										)
									return false
								})
							}
							{/* <div className={"bricks-grid--item bricks-grid--item__" + brickSize}>
								<div className="loadr-advert">THIS IS GOING TO BE AN ADVERT</div>
							</div> */}
						</div>
					</Col>
				</Row>
				<Row>
					<Col>
						<ButtonGroup aria-label="Pager" className="loadr-pager">
							{ !isLoading ? (
								<React.Fragment>
									{ pageNum === null ? (
										<Button as={NavLink} variant="dark" onClick={() => this.setState({ isLoading: true })} to={location.pathname + '?page=2'}>Next Page</Button>
										) : (
											<React.Fragment>
												{ Number(pageNum) === 2 ? (
													<Button as={NavLink} variant="dark" onClick={() => this.setState({ isLoading: true })} to={location.pathname}>Prev Page</Button>
													) : (
														<Button as={NavLink} variant="dark" onClick={() => this.setState({ isLoading: true })} to={location.pathname + '?page=' + Number(Number(pageNum) - 1)}>Prev Page</Button>
													)
												}
												{ (brickSize === 'small' && productCountSmall === pageProductLength) || (brickSize === 'large' && productCountLarge === pageProductLength) ? (
													<Button as={NavLink} variant="dark" onClick={() => this.setState({ isLoading: true })} to={location.pathname + '?page=' + Number(Number(pageNum) + 1)}>Next Page</Button>
													) : null
												}
											</React.Fragment>
										)
									}
								</React.Fragment>
								) : null
							}
						</ButtonGroup>
					</Col>
				</Row>
			</React.Fragment>
		)
	}

}

export default withRouter(props => <ProductList {...props} />);
