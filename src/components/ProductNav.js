// modules
import React, { Component } from 'react';
import { Sticky, StickyProvider } from 'react-stickup';
import { Row, Col, Nav } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';

class ProductNav extends Component {

    state = {
        types: [
            {name: "themes & templates", weight: 1},
            {name: "code", weight: 2},
            {name: "graphics", weight: 3},
            {name: "photos", weight: 4},
            {name: "videos", weight: 5},
            {name: "3d files", weight: 6},
            {name: "audio", weight: 7},
            {name: "courses", weight: 8}
        ],
    }

    render() {

        const { types } = this.state;

        return (
            <React.Fragment>
                <Row>
                    <Col>
                        <StickyProvider>
                            <Sticky defaultOffsetTop={99}>
                                {
                                    ({isSticky}) => (
                                        <Nav variant="pills" defaultActiveKey="/" className={isSticky ? 'nav-pills__stick': ''}>
                                            <Nav.Item><Nav.Link as={NavLink} exact to="/" className="nav-link--all">All</Nav.Link></Nav.Item>
                                            {
                                                types.map(type => {
                                                    return (
                                                        <Nav.Item key={type.weight}>
                                                            <Nav.Link
                                                                as={NavLink}
                                                                to={"/" + type.name.replace(/\s+/g, '-').replace(/&/g, 'and')}
                                                                className={"nav-link--" + type.name.replace(/&/g, '').replace(/templates/g, '').replace(/files/g, '')}>
                                                                {type.name}
                                                            </Nav.Link>
                                                        </Nav.Item>
                                                    )
                                                })
                                            }
                                        </Nav>
                                    )
                                }
                            </Sticky>
                        </StickyProvider>
                    </Col>
                </Row>
            </React.Fragment>
        )
    }

}

export default ProductNav;