import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { Container, Row, Col } from 'react-bootstrap';
// components
import Pixels from './elements/Pixels';

class ProductSpecific extends Component {

    render() {

        return (
            <React.Fragment>
                <div className="loadr-banner">
                    <Pixels color="themes" />
                    <Container fluid={true} className="loadr-banner--text">
                        <Row>
                            <Col>
                                <React.Fragment>
                                    <div className="breadcrumbs">
                                        <NavLink className="breadcrumb" to='/themes-and-templates'>Themes &amp; Templates</NavLink>
                                    </div>
                                    <h1>Product Specific</h1>
                                </React.Fragment>
                            </Col>
                        </Row>
                    </Container>
                </div>
            </React.Fragment>
        )

    }

}

export default ProductSpecific;
