// modules
import React from 'react';
import { Route, Switch, NavLink, useLocation } from 'react-router-dom';
import { Container, Navbar } from 'react-bootstrap';
// components
import ProductNav from './components/ProductNav';
import ProductList from './components/ProductList';
import ProductSpecific from './components/ProductSpecific';
// pages
import Home from './components/pages/Home';
import Themes from './components/pages/ThemesTemplates';
import Code from './components/pages/Code';
import Graphics from './components/pages/Graphics';
import Photos from './components/pages/Photos';
import Videos from './components/pages/Videos';
import ThreeD from './components/pages/3d';
import Audio from './components/pages/Audio';
import Courses from './components/pages/Courses';
// styles
import Logo from './images/loadr-logo.svg';
import './styles/app.scss';

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

const App = () => {

  // list variables
  let pageNum = useQuery().get('page');
  
  // specific variables
  const paths = window.location.pathname.split('/');
  let productName = paths[paths.length - 2],
      productID = paths[paths.length - 1];

  return (
    <React.Fragment>
      <Navbar>
        <Navbar.Brand as={NavLink} to="/">
          <img src={Logo} alt="loadr - Loads of Affordable Design Resources"/>
        </Navbar.Brand>
      </Navbar>
      <Switch>
        <Route exact path='/' component={Home} />
        <Route exact path='/themes-and-templates' component={Themes} />
        <Route exact path='/code' component={Code} />
        <Route exact path='/graphics' component={Graphics} />
        <Route exact path='/photos' component={Photos} />
        <Route exact path='/videos' component={Videos} />
        <Route exact path='/3d-files' component={ThreeD} />
        <Route exact path='/audio' component={Audio} />
        <Route exact path='/courses' component={Courses} />
      </Switch>
      {
        productName === undefined || productName === '' ? (
          <React.Fragment>
            <Container fluid={true} className="loadr-products-nav">
              <ProductNav />
            </Container>
            <Container fluid={true} className="loadr-products-canvas">
              <ProductList pageNum={pageNum} />
            </Container>
          </React.Fragment>
        ) : (
          <React.Fragment>
            <Container fluid={true} className="loadr-products-nav">
              <ProductNav />
            </Container>
            <ProductSpecific id={productID} />
          </React.Fragment>
        )
      }
    </React.Fragment>
  );

}

export default App;
